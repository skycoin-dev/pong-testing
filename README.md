# ping-tester

Simple golang webserver with one api, used for CI tests. This app will
start http server on `PING_SERVER_ADDR` (`:8080` by default) with one
endpoint `/ping` which responds with `pong` in body. Provided test
file makes `GET` request to this server and checks status and
body. Config file for drone is also provided.
