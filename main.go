package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("pong"))
	})

	addr := os.Getenv("PING_SERVER_ADDR")
	if addr == "" {
		addr = ":8080"
	}

	log.Print("Starting ping server on ", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
