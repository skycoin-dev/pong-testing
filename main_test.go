package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
)

func TestPing(t *testing.T) {
	addr := os.Getenv("PING_SERVER_ADDR")
	if addr == "" {
		addr = "localhost:8080"
	}

	res, err := http.Get(fmt.Sprintf("http://%s/ping", addr))
	if err != nil {
		t.Fatalf("Failed to perform GET request: %s", err)
	}

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Failed to perform GET request: invalid status %d", res.StatusCode)
	}

	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatalf("Failed to read response body: %s", err)
	}

	if string(data) != "pong" {
		t.Fatalf("Invalid response body: %s", string(data))
	}
}
